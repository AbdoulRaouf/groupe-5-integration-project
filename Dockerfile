# Utilisation de la version 3.8 de python sur un système ultra sécurisé
FROM python:3.8-alpine

# Définissons les variables d'environnement
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBUG 0

# installation de psycopg2 (driver python pour postgresql)
RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev \
    && apk add postgresql-dev \
    && pip install psycopg2 \
    && apk del build-deps



#=======================================================================================#
#                                   Gestion du backend                                  #
#=======================================================================================#
WORKDIR /app/

# Installation des dependences Python
COPY ./Pipfile /app/
RUN pip3 install --upgrade pip
RUN pip3 install pipenv
RUN pipenv install --skip-lock --system

# Ajouter le reste de code Backend
COPY . /app/

# Création de dossier de conservation des fichiers statics pour la production
RUN mkdir /app/staticfiles

# Collecte des fichiers statics
RUN python manage.py collectstatic --noinput

# ajouter un utilisateur non root et l'exécuter (pour plus de sécurité en ligne)
RUN adduser -D myuser
USER myuser

#EXPOSE Rendre le port du serveur de production $PORT disponible pour l'application
EXPOSE $PORT

# lancer le serveur gunicorn
CMD gunicorn core.wsgi:application --bind 0.0.0.0:$PORT
