# Projet d'intégration continue
![AppVeyor](https://img.shields.io/appveyor/build/themasterking/groupe-5-integration-project?branch=master)
![Codecov branch](https://img.shields.io/codecov/c/gitlab/themasterking/groupe-5-integration-project/master)
![licence](https://img.shields.io/badge/licence-MIT-green)
![python](https://img.shields.io/badge/python-3.6|3.7|3.8-informational)

[Groupe 5 CDI](https://fueltracker90.herokuapp.com/) projet de complet d'intégraton et de déploiement continue

## Configuration de python
_Installer python avant de commencer_

Récupérez le projet
````shell script
$ git clone https://gitlab.com/AbdoulRaouf/groupe-5-integration-project.git
````
Renommez le projet à cause des '-'
````shell script
$ rename groupe-5-integration-project groupe5_CI
````
Se déplacez dans le projet
````shell script
$ cd groupe5_CI
````
Mettez à jour pip _(gestionnaire officiel de paquet pour python)_
````shell script
$ python -m pip install --upgrade pip
````
Installer pipenv _(version améliorée de pip)_
````shell script
$ pip install pipenv
````
Initialisez l'environnement virtuel en installant django
````shell script
$ pipenv install django
````
Ceci va générer deux fichiers dont Pipfile _(contenant la liste de tous les dependances du projet)_
 et Pipfile.lock _(contenant les clés hachages des différentes dépendances)_
 ````shell script
$ ls
> Pipfile
> Pipfile.lock
> README.md
````
## Développement du projet avec django
Se connecter au shell virtuel du projet
````shell script
$ pipenv shell
````
Votre shell devrait ressembler à ceci
````shell script
(groupe5_CI-<xxxxx>) $ 
````
Initialiser le projet django à l'intérieur du répertoire _"groupe5_CI/"_
(à la place de 'core' mettez le nom de votre projet)
````shell script
(groupe5_CI-<xxxxx>) $ django-admin startproject core .
````
Maintenant l'arborescence de votre projet devrait ressembler à ceci
````shell script
groupe5_CI
  core/
      __init__.py
      asgi.py
      setting.py
      urls.py
      wsgi.py
  Pipfile
  Pipfile.lock
  README.md
````
Lancez le serveur
````shell script
(groupe5_CI-<xxxxx>) $ python manage.py runserver
````
Votre projet est accessible à l'adresse [localhost:8000](http://localhost:8000)

