from django.test import TestCase

from .models import Task

# Create your tests here.


class TaskTest(TestCase):
    """Test du model Utilisateur

    """

    name = "Configuration du cdi groupe 5"

    @classmethod
    def setUpTestData(cls):
        Task.objects.create(name=cls.name)

    def test_get_task(self):
        task = Task.objects.get(name=self.name)
        self.assertEqual(task.name, self.name)
