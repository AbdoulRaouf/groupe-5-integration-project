from django.db import models

# Create your models here.


class Task(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField(blank=True, null=True)
    begin_date = models.DateField(auto_now_add=True)
    end_date = models.DateField(blank=True, null=True)
